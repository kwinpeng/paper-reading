.. Paper Reading documentation master file, created by
   sphinx-quickstart on Tue Feb 11 16:59:48 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Paper Reading documentation!
=======================================

schedule note
-------------

.. toctree::
   :maxdepth: 2
   :caption: Schedule

   year2020

aio
---

.. toctree::
   :maxdepth: 2
   :caption: Research area of interest

   hpc

   system

   architecture

   algorithm

   application

archive
-------

.. toctree::
   :maxdepth: 1
   :caption: Archive

   mantainance


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
